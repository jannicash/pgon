/*
 * Copyright (c) 2006 Hyperic, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.hyperic.sigar.cmd;

import java.util.HashMap;

import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Swap;
import org.hyperic.sigar.SigarException;

/**
 * Display amount of free and used memory in the system.
 */
public class Free extends SigarCommandBase {

    public Free(Shell shell) {
        super(shell);
    }

    public Free() {
        super();
    }

    public String getUsageShort() {
        return "Display information about free and used memory";
    }

    private static Long format(long value) {
        return new Long(value / 1024);
    }
    
    /**
     * This function was added specifically for OpenWatch, 
     *  We are returning all the the 'free' data as a HashMap of String[].
     *  This function builds the String[] so that we can just iterate 
     *  over the objects and call returnHash.put();
     * @param obj
     * @return
     */
    private Long[] formArrayRow(Object[] obj){ 
    	Long[] ret = new Long[obj.length];
    	
    	for ( int i=0;i<obj.length;i++){
        	ret[i]=(Long) obj[i];
        }
    	
    	return ret;
    	
    }
    /**
     * In order to make a 'more effective' output command, I added a conditional
     * to the original to control whether this is screen printed or not.
     * 
     *  This 'output ( Sting[] args) function is only called by the internal 
     *  application to print all of the data directly to stdout
     *  
     *  If the data is desired in a data struct, see the non-null
     *  return variant of the output method.
     */
    public void output( String[] args )throws SigarException{
    	output(true,args);
    }
    
    /**
     * This method is designed to run the primary workload of the 'free'
     * application.  Calling it with a 'true' for the first argument will 
     * print the data to stdout.  Calling it with a false will print nothing
     * to the screen and only return the data to the calling method.
     * @param print
     * @param args
     * @return
     * @throws SigarException
     */
    public HashMap<String, Long[]> output(boolean print,String[] args) throws SigarException {
       HashMap<String,Long[]> ret = new HashMap<String, Long[]>();
       
    	Mem mem   = this.sigar.getMem();
        Swap swap = this.sigar.getSwap();

        Object[] header = new Object[] { "total", "used", "free" };

        Object[] memRow = new Object[] {
            format(mem.getTotal()),
            format(mem.getUsed()),
            format(mem.getFree())
        };

        Object[] actualRow = new Object[] {
            format(mem.getActualUsed()),
            format(mem.getActualFree())
        };

        Object[] swapRow = new Object[] {
            format(swap.getTotal()),
            format(swap.getUsed()),
            format(swap.getFree())
        };

        /*
         * Added specifically for OpenWatch.  The print should only 
         * occur if it is specified by the caller
         */
        if ( print ) {
	        printf("%18s %10s %10s", header);
	
	        printf("Mem:    %10ld %10ld %10ld", memRow);
	                
	        
	        //e.g. linux
	        if ((mem.getUsed() != mem.getActualUsed()) ||
	            (mem.getFree() != mem.getActualFree()))
	        {
	            printf("-/+ buffers/cache: " + "%10ld %10d", actualRow);
	        }
	
	        printf("Swap:   %10ld %10ld %10ld", swapRow);
	
	        printf("RAM:    %10ls", new Object[] { mem.getRam() + "MB" });
        }
        
        /*
         * Added by OpenWatch, return the data 
         * as an in-memory structure for simplified push to 
         * a database.
         */
        ret.put("Mem",formArrayRow(memRow));
        ret.put("-/+ buffers/cache",formArrayRow(actualRow));
        ret.put("Swap",formArrayRow(swapRow));
        
        return ret;
    }

    public static void main(String[] args) throws Exception {
        new Free().processCommand(args);
    }
}
