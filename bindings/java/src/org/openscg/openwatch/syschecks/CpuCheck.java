package org.openscg.openwatch.syschecks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import org.hyperic.sigar.cmd.CpuInfo;

public class CpuCheck extends Checks {

	String insert = "INSERT INTO snapshots.snap_cpu ( snap_id, cpu_id, usertime, "
				  + "systime, idletime, waittime, nicetime, combined, irqtime, "
				  + "softirqtime, stolentime) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	
	ArrayList<String[]> cpuData = null;
	
	@Override
	public PreparedStatement saveData(int snap_id, Connection conn) {

		batch=true;
		
		PreparedStatement st = null;
		
		try{ 
			st = conn.prepareStatement(insert);
			
			st.setInt(1, snap_id);
			
			for(String[] aCpu : cpuData ){
				
				for ( int i=0; i<aCpu.length;i++ ) { 
				//	System.out.println("Key: " + i + " Val: " + aCpu[i]);
					
					if ( i == 0 ) {
					
						st.setInt(i+2,Integer.parseInt(aCpu[i]));
					}
					
					else{
						Double d = null;
						if ( " ".equals(aCpu[i]) ){
							d = (double)-1;
						}
						else{
							d = Double.parseDouble(aCpu[i]);
						}
							
						st.setDouble(i+2, d);
					}
				}
				
				st.addBatch();
			}
			st.executeBatch();
			st.close();
		}catch (Exception e ) { 
			e.printStackTrace();
		}
		
		return st;
	}

	@Override
	public void getData() {
		CpuInfo cpui = new CpuInfo();

		try {
			cpuData = cpui.getCpuUsageData();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
