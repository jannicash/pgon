package org.openscg.openwatch.syschecks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;

import org.hyperic.sigar.SigarException;
import org.hyperic.sigar.cmd.Free;

public class MemoryCheck extends Checks{

	/*
	 * Here, we are a hash of 'row name, array of longs'
	 */
	HashMap<String,Long[]> data;
	
	String insert = "INSERT INTO snapshots.snap_mem ( snap_id, dttm,metric,"
				  + "total, free, used ) VALUES ( ?,now(),?,?,?,?)";
	@Override
	public PreparedStatement saveData(int snap_id, Connection conn) {
		PreparedStatement st = null;
		
		try{ 
			st = conn.prepareStatement(insert);
			
			for ( String key : data.keySet()){

				st.setInt(1, snap_id);
				st.setString(2,key);
				
				/*
				 * There's an oddity about the output of 'free' that we have 
				 * to deal with here.  There is one line ( header: -/+ buffers/cache )
				 * this line only has two columns in it, one is for used ,one is 
				 * for free ( essentially, no totals column ).  Sample output:
				 *
				 *              total       used       free
				 *   Mem:      16777216   12687484    4089732
                 *   -/+ buffers/cache:    9519096    7258120
                 *   Swap:            0          0          0
				 *   RAM:       16384MB
				 *   
				 *    The buffers / cache line is effectively a metric based
				 *    values from the above row.  In order to deal with this,
				 *    I'm just going to assume that any data back from 'free' 
				 *    with less than 3 rows is this row and should be shifted 
				 *    appropriately.
				 */				
				if ( data.get(key).length >= 3){
					st.setLong(3, (data.get(key)[0]));
					st.setLong(4, (data.get(key)[1]));
					st.setLong(5, (data.get(key)[2]));
				}else{
					st.setNull(3,java.sql.Types.NUMERIC);
					st.setLong(4, (data.get(key)[0]));
					st.setLong(5, (data.get(key)[1]));
				}
					
				/* Let's commit the 4 rows in one-go */
				st.addBatch();
			}
			
			/* Execute all 4 inserts */
			st.executeBatch();
			st.close();
			
		}catch ( Exception e) { 
			e.printStackTrace();
		}
		
		return st;		
	}

	@Override
	public void getData() {
		// TODO Auto-generated method stub
		Free f = new Free();
		
		try {
			data = f.output(false, new String[0]);
		} catch (SigarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
