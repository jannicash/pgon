package org.openscg.openwatch.syschecks;

import java.sql.Connection;
import java.sql.PreparedStatement;

public abstract class Checks {

	boolean batch = false;
	
	public void save (int snap_id, Connection conn){
		try { 
			if ( conn != null ) { 
				getData();
				saveData(snap_id, conn);
			}
		}catch ( Exception e ) { 
			e.printStackTrace();
		}
	}
	
	public abstract PreparedStatement saveData(int snap_id, Connection conn);
	
	public abstract void getData();
}
