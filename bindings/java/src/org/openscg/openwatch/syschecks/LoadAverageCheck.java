package org.openscg.openwatch.syschecks;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.hyperic.sigar.SigarException;
import org.hyperic.sigar.cmd.Uptime;

public class LoadAverageCheck extends Checks{
	
	String insert = " INSERT INTO snapshots.snap_load_avg "
				  + "( snap_id, load5,load10,load15 ) "
				  + "VALUES ( ?,?,?,? )";

	double [] loadAvg = null;
	
	@Override
	public PreparedStatement saveData(int snap_id, Connection conn) {
		PreparedStatement st = null;
		
		try { 
			
			st = conn.prepareStatement(insert);
			st.setInt(1, snap_id);
			
			for ( int i = 0; i< loadAvg.length ; i++ ){
				//System.out.println("Index: " + (i+2) + " Val: " + loadAvg[i]);
				 
				st.setDouble(i+2, loadAvg[i]);
			}
		
			st.execute();
			st.close();
			
		}catch ( Exception e ) { 
			e.printStackTrace();
		}
		
		return st;
	}

	@Override
	public void getData() {
		// TODO Auto-generated method stub
		Uptime loadAverage = new Uptime();
		
		try {
			loadAvg = loadAverage.getLoadAvg();
		} catch (SigarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	
	
}
