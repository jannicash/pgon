package org.openscg.openwatch.syschecks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import org.hyperic.sigar.SigarException;
import org.hyperic.sigar.cmd.Iostat;

public class IostatCheck extends Checks{

	String insert = "INSERT INTO snapshots.snap_iostat "
			      + "( snap_id, dttm, filesystem, mountpoint, reads, "
			      + "writes, rbytes, wbytes, queue, svctm ) "
			      + "VALUES ( ?, now(), ?, ?, ?, ?, ?, ?, ?, ? )";
	
	ArrayList<String[]> data = null;

	@Override
	public PreparedStatement saveData(int snap_id, Connection conn) {
		
		PreparedStatement st=null;

		try {

			st = conn.prepareStatement(insert);
			
			st.setInt(1, snap_id);
			for ( String[] line : data ) { 
				for ( int i = 0 ; i< line.length ; i++){
					
					/* +1 to avoid snap_id */
					if ( i == 0 || i == 1){
						st.setString(i+2, line[i]);
					}
					else {
						Double t = null;
						if ("-".equals(line[i])){ 
							t = (double)-1;
						}
						else{
							t = Double.parseDouble(line[i]);
						}
							
						st.setDouble(i+2, t);;
					}		
				}
				st.addBatch();
			}
			
			st.executeBatch();
			
			st.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return st;
	}

	@Override
	public void getData() {
		Iostat io = new Iostat();
		
		try {
			data = io.getIostatData();
			
		} catch (SigarException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
