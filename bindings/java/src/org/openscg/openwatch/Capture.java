/* Version 1.7 */
package org.openscg.openwatch;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.openscg.openwatch.syschecks.CpuCheck;
import org.openscg.openwatch.syschecks.IostatCheck;
import org.openscg.openwatch.syschecks.LoadAverageCheck;
import org.openscg.openwatch.syschecks.MemoryCheck;
import org.openscg.openwatch.utils.Arguments;
/**
 * OpenWatch is an open-source utility for monitoring
 * PostgreSQL database servers.  Combined with a 
 * script installed in the database, this tool will 
 * monitor database internals as well as system resources
 * 
 * @author scottm
 *
 */
public class Capture {

	static String productName = "OpenWatch";
	static Connection conn = null;
	
	/* User Arguments */
	static String url = null;
	static String user = null;
	static String password = null;
	static boolean dbOnly = false;
	static boolean sysOnly = false;
	static boolean keepIsDays = false;
	static int numToKeep = -1;
	
	
	/**
	 * @author scottm
	 * Entry point to OpenWatch capture
	 */
	public static void main ( String[] args ) { 
		
		int snap_id = -1;
		
		System.out.println("Initializing " + productName);
		
		processArguments(args);
	
		System.out.println("Executing snapshots..." ) ;

		System.out.print("\tConnecting to database...\t" );

		connectToDb();

		System.out.println(" DONE");
		
		System.out.print("\tValidating snapshot storage...\t");
		
		/*
		 * Make sure that the snapshot schema appears to be installed
		 */

		if ( ! checkSnapshotsInstalled() ) { 
			System.out.println("FAILED!");
			System.out.println("\n\nPlease install the snapshots "
							 + "schema in the " );
			System.out.println("     database referred to by " + url ) ; 
			System.out.println("     and re-run the command");
			System.exit(1);

		}else{ System.out.println(" DONE"); }

		if ( numToKeep > 0 ) { 
			
			System.out.println("\tPruning old snapshots...");
			System.out.println("\t  Will keep " + numToKeep 
					+ ((keepIsDays) ? " Days of":"") + " Snapshots" );
			pruneSnapshots();
			System.out.println(" DONE");
			
		}
		
		/* If the user only wants system checks, don't perform db checks*/
		if ( ! sysOnly ) {
			System.out.print("\tExecuting database checks...\t");
	
			snap_id = doDbChecks();
	
			System.out.println(" DONE");
		}else{
			/* 
			 * If the user only wants system checks, we need to get a new 
			 *  snap_id from the sequence on the database.  Normally, this
			 *  is generated for us by the 'save_snap()' stored procedure.
			 *  If we're not executing that, do it manually
			 */
			snap_id = getSnapId();
		}

		/*
		 * If the snap_id isn't a positive, non-zero number, 
		 *  then there is a problem
		 */
		if ( snap_id <= 0 ) { 
			System.out.println("Warning, snapshot sequencing is broken.") ;
			System.out.println("Please fix or re-install snapshot schema" ) ;
			System.exit(1);
		}
		
		/* If the user only wants db checks, don't perform system checks*/
		if ( ! dbOnly ) {
			System.out.print("\tExecuting system checks...\t");
	
			doSysChecks(snap_id);
	
			System.out.println(" DONE");
		}
		
		System.out.println("Snapshot Complete!");
	}
	
	/**
	 * Prune old snapshots from the system 
	 * 
	 */
	private static void pruneSnapshots() {

		/*
		 * This function shouldn't even be called if this 
		 * condition is violated.  But, I like to wear a belt, just in
		 * case
		 */
		if ( numToKeep <= 0 ) { 
			return;
		}

		try{
			String query = null;
			String deleteQuery = null;
			String infoQuery = null;
			/*
			 * Technically, this method will only be called 
			 * if numToKeep is > 0.  But this is the suspenders
			 * for my belt.
			 */
			if ( numToKeep > 0 ) { 
				infoQuery = "SELECT min(snap_id), max(snap_id) FROM "
						  + "snapshots.snap WHERE ";
				
				deleteQuery = "DELETE FROM snapshots.snap WHERE ";
			
				if ( keepIsDays ) { 
					query = "dttm <= now() - '"+ numToKeep 
						   + " days'::interval ";
				}
				else
				{
					query = "snap_id <= ( SELECT " 
						   + "(MAX(snap_id) - "+numToKeep+") "
						   + "from snapshots.snap )";
				}
				
				//System.out.println("Delete: " + query);
			}
			
			deleteQuery += query;
			infoQuery += query;

			PreparedStatement st = conn.prepareStatement(infoQuery);
			ResultSet rs = st.executeQuery();
			
			int max = 0;
		    int min = 0;
			
			while ( rs.next() ){
				max = rs.getInt("max");
				min = rs.getInt("min");
			}
			
			String message = "\t  Removing snapshot" 
						   +  (min == max ? ": "+max 
								   : "s " + min + " - " + max)    ;
			
			System.out.println(message);
			System.out.print("\t\t\t\t ...\t");
			rs.close();
			st.close();

			st = conn.prepareStatement(deleteQuery);
			st.execute();
			st.close();
			
		}catch ( Exception e ) { 
			e.printStackTrace();
			System.exit(1);
		}
	}
	/**
	 * Manually grab a new snap_id from the database sequence
	 * @return -1 ( if failure ) or new snap_id
	 */
	private static int getSnapId() {
		int ret = -1;
		try{
			PreparedStatement st = conn.prepareStatement(
										"SELECT nextval('snapshots.snap_seq')");
			ResultSet rs = st.executeQuery();
			while ( rs.next() ) { 
				ret = rs.getInt(1);
			}
			st.close();
			st = conn.prepareStatement(
								"INSERT INTO snapshots.snap (snap_id, dttm ) "
							   +"VALUES ( ?,now())" );
			st.setInt(1, ret);
			st.execute();
		}catch ( Exception e ) { 
			e.printStackTrace();
			System.exit(1);
		}
		return ret;
	}

	/**
	 * Run the system checks
	 * @param snap_id
	 */
	static void doSysChecks(int snap_id){
	
		CpuCheck cpc = new CpuCheck();
		cpc.save(snap_id, conn);
		
		LoadAverageCheck lac = new LoadAverageCheck();
		lac.save(snap_id, conn);
		
		IostatCheck ioc = new IostatCheck();
		ioc.save(snap_id, conn);
		
		MemoryCheck mc = new MemoryCheck();
		mc.save(snap_id, conn);
	}
	
	static void decryptConfig (File encryptedFile){
		//TODO
	}
	/** 
	 * Run the database checks
	 * @return
	 */
	static int doDbChecks() {
		int snap_id = -1;

		try { 
			PreparedStatement st = conn.prepareStatement(
					                    "SELECT snapshots.save_snap()");
			ResultSet rs = st.executeQuery();
			
			while ( rs.next() ) { 
				snap_id = rs.getInt("save_snap");
			}
			
			st.close();
			
		}catch ( Exception e ) { 
			e.printStackTrace();
		}
		
		return snap_id;
	}
	
	/**
	 * Process all the valid arguments for the Capture program
	 * @author scottm
	 * @param args
	 */
	static void processArguments ( String[] args ) { 
		
		if ( args.length <= 0 ){
			printHelpAndExit("No options supplied");
		}
		
		for ( int i=0 ; i<args.length ; i++ ){
			
			Arguments arg = null;
			
			try { 
				arg = Arguments.valueOf(
						            args[i].replaceAll("-", "").toUpperCase());
				
			}catch(IllegalArgumentException iae){
				printHelpAndExit("Invalid option: " + args[i] + "\n");
			}
			
			switch ( arg ) {
				case URL : url = args[++i];
					break;
				case USER : user = args[++i];
					break;
				case PASS : password = args[++i];
					break;
				case DBONLY : dbOnly = true;
					break;
				case SYSONLY : sysOnly = true;
					break;
				case KEEPDAYS : keepIsDays = true;
								numToKeep = Integer.parseInt(args[++i]);
					break;
				case KEEPSNAPS : keepIsDays = false;
								 numToKeep = Integer.parseInt(args[++i]);
					break;			
				case HELP : printHelpAndExit("Available Options");
					break;
				default : printHelpAndExit("Invalid option: " + args[i]);
			}
		}
		if ( url == null ) { 
			printHelpAndExit("You must supply a database "
						   + "url with the --url option");
		}
	}

	/**
	 * Connect to the database.  Exit(1) otherwise with a message
	 * @return If connection was successful
	 */
	static boolean connectToDb() { 

		boolean connected = false;

		try { 
			conn = DriverManager.getConnection(url,user,password);
			if ( conn != null ) {
				connected = true;
			}
//			PreparedStatement st = conn.prepareStatement
	//				               ("set application_name=\""+productName+"\"");
	//		st.execute();
	//		st.close();
		}catch ( Exception e ) { 
			System.out.println("\nCannot connect to database,exiting...");
			e.printStackTrace();
			connected = false;
			System.exit(1);	
		}
		
		return connected;
		
	}

	/**
	 * Determines if the snapshots schema has been installed.  This isn't necessarily
	 * a perfect check, it only validates if the snapshots.snap table has been loaded in
	 * to the database.  It is possible that either the db snaps or system snaps 
	 * ( or any combination of them have not been loaded properly ).  This will be caught
	 * as an exception during the actual snapshot with a SQL missing table error
	 * @return If a snapshots schema with the main snap table exists ( TRUE / FALSE )
	 */
	static boolean checkSnapshotsInstalled () { 
		boolean ret = true;
		
		try{
			PreparedStatement st = conn.prepareStatement(
									"SELECT * FROM snapshots.snap limit 1");
			st.execute();
			st.close();
			
		}catch ( Exception e ) { 
			e.printStackTrace();
			ret = false;
		}
		
		return ret;
	}

	static void printHelpAndExit(String msg){
		printHelp(msg);
		System.exit(1);
	}
	static void printHelp(String msg ) { 
		System.out.println("\n" + msg );
		printHelp();
	}
	static void printHelp() { 
		
		System.out.println(productName + " Options ");
		System.out.println("-------------------");
		for ( Arguments arg: Arguments.values() ){
			System.out.printf("\t%-15s %s\n","--" 
					          + arg.toString().toLowerCase(),
					            arg.getHelp_txt() );
		}
	}
}
