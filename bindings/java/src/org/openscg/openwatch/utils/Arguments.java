package org.openscg.openwatch.utils;

public enum Arguments {

	
	URL("The jdbc URL for the database, e.g. jdbc:postgresql://192.168.1.4:5432/postgres"),
	USER ("The username to connect to the database with"),
	PASS ("The password to connect to the database with"),
	DBONLY ("Only execute database checks, no system checks"),
	SYSONLY ("Only execute system checks, no database checks"),
	KEEPDAYS("How many days of snapshots to keep"),
	KEEPSNAPS("How many snapshots to keep"),
	HELP("Print Help and exit"),
	;
	
	private String help_txt = null;

	Arguments (String _help_txt){
		this.setHelp_txt(_help_txt);
	}
	public String getHelp_txt() {
		return help_txt;
	}

	public void setHelp_txt(String help_txt) {
		this.help_txt = help_txt;
	}
}
