#!/bin/sh

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )


test -d "$DIR/../java/jre"
if [ $? -eq 0 ]
then
    export JAVA_HOME=$DIR/../java/jre/
    export PATH=$JAVA_HOME/bin:$PATH
fi

java -jar $DIR/../lib/openwatch.jar $@ 

