#!/bin/sh


# This is an example of how to call openwatch 
# to execute a snapshot for you

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

export JAVA_HOME=$DIR/../java/jre/
export PATH=$JAVA_HOME/bin:$PATH

java -jar $DIR/../lib/openwatch.jar --url jdbc:postgresql://localhost:9290/postgres --user scottm --pass joe

